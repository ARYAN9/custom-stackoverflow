<?php

namespace App\Http\Controllers;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class QuestionsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth'])->only(['create','store','edit','update']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('owner')->latest()->paginate(10);//eager load
        return view('questions.index',compact(['questions']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        app('debugbar')->disable();
        return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title'=>$request->title,
            'body'=>$request->body
        ]);
        session()->flash('success',"Questions has been added successfully!");
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    /**
     * public function show($slug){
     *  $question = Question::where('slug',$slug)->firstOrFail();
     * }
     * The above in hackton!!!
     * //RouteModel will automatically be binded as we have configured RouteServiceProvider.php for slug
     */
    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show',compact(['question']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        // if(Gate::allows('update-question',$question)){
        //     app('debugbar')->disable();
        //     return view('questions.edit',compact(['question']));            
        // }
        if($this->authorize('update',$question)){
            app('debugbar')->disable();
            return view('questions.edit',compact(['question']));            
        }
        abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        // if(Gate::allows('update-question',$question)){
        //     $question->update([
        //         'title'=>$request->title,
        //         'body'=>$request->body
        //     ]);
        //     session()->flash('success','Questions has been updated succesfullt');
        //     return redirect(route('questions.index'));
        // }
        if($this->authorize('update',$question)){
            $question->update([
                'title'=>$request->title,
                'body'=>$request->body
            ]);
            session()->flash('success','Questions has been updated succesfullt');
            return redirect(route('questions.index'));
        }

        // $question->update([
        //     'title'=>$request->title,
        //     'body'=>$request->body
        // ]);
        abort(403);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        // if(auth()->user()->can('delete-question',$question)){
        //     $question->delete();
        //     session()->flash('success','Question has been deleted successfully!');
        //     return redirect(route('questions.index'));    
        // }
        if($this->authorize('delete',$question)){
            $question->delete();
            session()->flash('success','Question has been deleted successfully!');
            return redirect(route('questions.index'));    
        }
        abort(403);
    }
}
