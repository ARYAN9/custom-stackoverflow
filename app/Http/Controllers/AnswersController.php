<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\Answer\CreateAnswerRequest;
use App\Http\Requests\Answer\UpdateAnswerRequest;
use App\Http\Requests\Answers\CreateAnswerRequest as AnswersCreateAnswerRequest;
use App\Notifications\NewReplyAdded;
use App\Question;
use Illuminate\Http\Request;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAnswerRequest $request,Question $question)
    {
        $question->answers()->create([
            'body'=>$request->body,
            'user_id'=>auth()->id()
        ]);

        $question->owner->notify(new NewReplyAdded($question));

        session()->flash('success', 'Your answer submitted succesfully!');
        return redirect($question->url);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question,Answer $answer)
    {
        $this->authorize('update',$answer);
        return view('answer.edit',compact([
            'question',
            'answer'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAnswerRequest $request,Question $question, Answer $answer)
    {
        $answer->update([
            'body'=>$request->body
        ]);
        session()->flash('success','Answer updated successfully');
        return redirect($question->url);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question,Answer $answer)
    {
        $this->authorize('delete',$answer);
        $answer->delete();
        session()->flash('success','Answer deleted successfully');
        return redirect($question->url);
    }

    public function bestAnswer(Request $request,Answer $answer){
        $this->authorize('markAsBest',$answer);//naitho 403 if fail
        $answer->question->markBestAnswer($answer);
        return redirect()->back();
    }
}
